Name:           sshguard
Version:        2.3.1
Release:        1%{?dist}
Summary:        Brute force detector for SSH, Exim, VSFTPD and more. Blocks by ip with firewalld

License:        BSD and GPLv2+ and Public Domain
URL:            https://www.sshguard.net
Source0:        https://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz
Source1:        sshguard.service
Source2:        sshguard.conf

BuildRequires:  autoconf automake byacc flex gcc python-docutils make
Requires:       firewalld
%{?systemd_requires}
BuildRequires: systemd

%description
sshguard protects hosts from brute-force attacks against SSH and other services. 
It aggregates system logs and blocks repeat offenders using one of several firewall backends.

%prep
%setup -q

%configure
%{configure} --bindir=%{_bindir} --sbindir=%{_sbindir} --libexecdir=%{_libexecdir} --with-firewall=firewalld

%build
make %{?_smp_mflags}

%install
make install DESTDIR=$RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_unitdir}
mkdir -m 0755 -p $RPM_BUILD_ROOT/%{_sysconfdir}/%{name}.d
mkdir -m 0700 -p $RPM_BUILD_ROOT/%{_localstatedir}/db/%{name}
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT/%{_unitdir}/%{name}.service
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT/%{_sysconfdir}/%{name}.conf

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%doc 
%defattr(-,root,root,-)
%files
%{_mandir}/man8/%{name}*
%{_mandir}/man7/%{name}*
%config(noreplace) %{_sysconfdir}/%{name}.conf
%{_unitdir}/%{name}.service
%{_localstatedir}/db/%{name}
%{_sbindir}/%{name}
%{_libexecdir}/sshg-blocker
%{_libexecdir}/sshg-parser
%{_libexecdir}/sshg-logtail
%{_libexecdir}/sshg-fw-firewalld
%{_libexecdir}/sshg-fw-hosts
%{_libexecdir}/sshg-fw-ipfw
%{_libexecdir}/sshg-fw-ipset
%{_libexecdir}/sshg-fw-nft-sets
%{_libexecdir}/sshg-fw-iptables
%{_libexecdir}/sshg-fw-ipfilter
%{_libexecdir}/sshg-fw-null
%{_libexecdir}/sshg-fw-pf

%clean
rm -rf $RPM_BUILD_ROOT

%changelog
* Thu Jul  5 2018 root
- 